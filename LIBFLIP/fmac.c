/* ---------------------------------------------------------------- */
/** 
    \file fmac.c
    \brief Code for the fused multiply-add accumulate operation.
*/
/* ---------------------------------------------------------------- */

#include "flip.h"
#include "internal/internal.h"

/** 
    \brief FP \b fused multiply-add accumulate operation: returns \f$ x \times y + z \f$.

    \param x is the multiplicand (32-bit FPN)
    \param y is the multiplier (32-bit FPN)
    \param z is the addend (32-bit FPN)

    \addindex FPN

*/

inline
float32 __float32_flip_fmadd_float32_float32_float32(float32 z, float32 y, float32 x)
{
  float32 _x, _y, _z;
  sbits32 _xBExp, _yBExp, _zBExp, _prodBExp, _rBExp, _xBExp_val_spl, _yBExp_val_spl, _zBExp_val_spl;

  bits32 _xFrac, _yFrac, _zFrac, _xMant, _yMant, _zMant, _rMant;
  bits64 _prodMant64, _zMant64, _rMant64;
  sbits64 _prodsMant64, _zsMant64, _rsMant64, _tempsMant64;

  bits32 _prodSign, _zSign, _rSign, _lzc;

  bits32 _xMant_L, _xMant_H, _yMant_L, _yMant_H;
  bits32 _xHyH, _xHyL, _xLyH, _xLyL;
  bits32 _xyM;
  bits32 _prod_high32;

  sbits32 _expDiff, _expDiffAbs, _preShift;

  _x = x;
  _y = y;
  _z = z;

  _xFrac = extractFloat32Frac( _x );
  _xBExp = ( sbits32 ) extractFloat32Exp( _x );

  _yFrac = extractFloat32Frac( _y );
  _yBExp = ( sbits32 ) extractFloat32Exp( _y );

  _zFrac = extractFloat32Frac( _z );
  _zBExp = ( sbits32 ) extractFloat32Exp( _z );

  _zSign = extractFloat32Sign( _z );

  _xMant = _xFrac | ( 1 << _FLIP_MANT_SIZE );
  _yMant = _yFrac | ( 1 << _FLIP_MANT_SIZE );
  _zMant = _zFrac | ( 1 << _FLIP_MANT_SIZE );

  if( _zBExp == 0 )
    _zMant = 0;

  _prodBExp = _xBExp + _yBExp - 0x7F;
  _expDiff = _prodBExp - _zBExp;
  _prodSign = ( _x ^ _y ) & 0x80000000;


  _xMant_L = _xMant & 0xFFFF;
  _yMant_L = _yMant & 0xFFFF;
  _xMant_H = _xMant >> 16;
  _yMant_H = _yMant >> 16;

  _xLyL = _xMant_L * _yMant_L;
  _xLyH = _xMant_L * _yMant_H; 
  _xHyL = _xMant_H * _yMant_L; 
  _xHyH = _xMant_H * _yMant_H; 

  // sum up the two 'middle' products
  _xyM = _xLyH + _xHyL;

  _prod_high32 = ( _xyM + ( _xLyL >> 16 ) + ( _xHyH << 16 ) );
  
  _prodMant64 = ( ( ( bits64 ) _prod_high32 << 32 ) | ( _xLyL << 16 ) ) >> 2;
  _zMant64 = ( bits64 ) _zMant << 37;

  _prodsMant64 = ( sbits64 ) _prodMant64;
  _zsMant64 = ( sbits64 ) _zMant64;

  if( _prodSign ) _prodsMant64 = -_prodsMant64;
  if( _zSign ) _zsMant64 = -_zsMant64;

  _tempsMant64 = _prodsMant64;
  if( _expDiff < 0 )
    {
      _prodsMant64 = _zsMant64;
      _zsMant64 = _tempsMant64;
    }

  _expDiffAbs = abs( _expDiff );
  _preShift = _FLIP_MIN( _expDiffAbs, 48 );

  _zsMant64 = ( _zsMant64 >> _preShift ) | ( ( _zsMant64 & ~( ~0 << _preShift ) ) != 0 );

  _rsMant64 = _prodsMant64 + _zsMant64;

  _rSign = ( bits32 ) ( _rsMant64 >> 32 ) & 0x80000000;

  _rMant64 = ( bits64 ) __absl( _rsMant64 );

  _lzc = ( bits32 ) countLeadingZeros64( _rMant64 );
  _rMant64 = _rMant64 << _lzc;
  _rBExp = _FLIP_MAX( _prodBExp, _zBExp ) + 3 - _lzc;

  _rMant = ( bits32 ) ( ( _rMant64 >> 38 ) | ( ( _rMant64 & 0x3FFFFFFFFFLL ) != 0 ) );
  _rBExp = _rBExp + _flip_detect_exp_update( _rMant, _rSign, 26 );

  _rMant = _flip_round( _rMant, _rSign );

  _xBExp_val_spl = ( _xBExp - 1 ) & 0xFE;
  _yBExp_val_spl = ( _yBExp - 1 ) & 0xFE;
  _zBExp_val_spl = ( _zBExp - 1 ) & 0xFE;

  if( _FLIP_MAX( _FLIP_MAX( _xBExp_val_spl, _yBExp_val_spl ), _zBExp_val_spl ) == 0xFE )
    {
      if( ( _z << 1 ) > 0xFF000000 )
	return _flip_declare_nan();

      _prodBExp = _prodBExp + ( _prod_high32 >> 31 );
      _zSign = _zSign << 31;

      if( _xBExp == 0xFF )
	{
	  if( _xFrac || ( _yBExp == 0 ) )
	    {
	      return _flip_declare_nan();
	    }
	  else
	    {
	      if( ( ( _y << 1 ) > 0xFF000000 ) || ( ( _yBExp == 0 ) ) )
		return _flip_declare_nan();
	      if( _zBExp == 0xFF )
		{
		  if( _zFrac || ( _prodSign != _zSign ) )
		    return _flip_declare_nan();
		}

	      return _flip_declare_infinity( _prodSign );
	    }
	}

      if( _xBExp == 0 )
	{
	  if( _yBExp == 0xFF )
	    return _flip_declare_nan();
	  if( _zBExp == 0 )
	    return ( _prodSign & _z ) & 0x80000000;
	  return _z;
	}

      if( _yBExp == 0xFF )
	{
	  if( _yFrac )
	    return _flip_declare_nan();
	  if( _zBExp == 0xFF )
	    {
	      if( _zFrac || ( _prodSign != _zSign ) )
		return _flip_declare_nan();
	    }

	  return _flip_declare_infinity( _prodSign );
	}

      if( _yBExp == 0 )
	{
	  if( _zBExp == 0 )
	    return ( _prodSign & _z ) & 0x80000000;
	  return _z;
	}

      if( _zBExp == 0xFF )
	{
	  if( _zFrac || ( ( _prodBExp == 0xFF ) && ( _prodSign != _zSign ) ) )
	    return _flip_declare_nan();

	  return _rSign | ( _z & 0x7FFFFFFF );
	}

    }

  if( _rMant == 0 ) 
    return 0;
  
  if( _rBExp <= 0 )
    {
      return _rSign | _flip_underflow( _rSign );
    }

  if( _rBExp >= 255 )
    return _rSign | 0x7F800000;

  return _rSign | ( _rBExp << _FLIP_MANT_SIZE ) | ( _rMant & 0x007FFFFF );

}


inline
float32 __float32_flip_fmsub_float32_float32_float32( float32 z, float32 y, float32 x )
{
  z = z ^ 0x80000000 ;
  return __float32_flip_fmadd_float32_float32_float32( z, y, x );
}

inline
float32 __float32_flip_fnmadd_float32_float32_float32( float32 x, float32 y, float32 z )
{
  return __float32_flip_fmadd_float32_float32_float32( x, y, z ) ^ 0x80000000;
}

inline
float32 __float32_flip_fnmsub_float32_float32_float32( float32 z, float32 y, float32 x )
{
  z = z ^ 0x80000000 ;
  return __float32_flip_fnmadd_float32_float32_float32( z, y, x );
}

