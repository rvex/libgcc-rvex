r-VEX libgcc library
====================

This repository builds libgcc for the r-VEX. This library contains software
floating point and integer division functions. The GCC front-end of Open64 will
automatically generate references to these functions whenever it cannot expand
an operation to an instruction.


Configuration and build process
-------------------------------

Configuring and building libgcc works like this:

 - `rvex-cfg.py <json>`. This stage configures the hardware division assembly
   file for the appropiate number of lanes. This is automatically run by the 
   r-VEX toolchain generator, in which case the JSON file is generated based on
   a user-friendly INI file. If you're building manually, you can run
   `rvex-cfg.py -` instead to generate the required files with default values
   (8-issue).
 - `configure`. Generates a Makefile for the later steps. Allows you to set a
   custom build directory (by running the `configure` command from another
   working directory) and a custom installation directory (defaults to
   `install`). You also need to set the toolchain root directory using the
   `--tooldir` flag.
 - `make`. Compiles the library.
 - `make install`. Installs the library to the specified `install` directory.

