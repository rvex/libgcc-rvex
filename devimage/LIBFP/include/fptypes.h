#ifndef __FPTYPES_H__
#define __FPTYPES_H__

/*------------------------------------------------------------------------------
Type definitions mandatory to use operators.
This file is derived from fpmodels.src 
($Revision: 1.36.2.1 $ $Date: 2010/09/03 11:42:55 $).
STMicroelectronics Confidential.
------------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/



typedef int flag;
typedef int uint8;
typedef int int8;
typedef unsigned char bits8;
typedef char sbits8;

typedef unsigned short bits16;
typedef short sbits16;
typedef short int16 ;
typedef short fract16 ;
typedef unsigned short uint16 ;

typedef unsigned bits32;
typedef int sbits32;
typedef int int32 ;
typedef int fract32 ;
typedef unsigned int uint32 ;

#if defined(__ST100) || defined(__ST122)
typedef unsigned long bits40;
typedef long sbits40;
typedef long int40 ;
typedef long fract40 ;
typedef unsigned long uint40 ;
#else
typedef unsigned long long bits40;
typedef long long sbits40;
typedef long long int40 ;
typedef long long fract40 ;
typedef unsigned long long uint40 ;
#endif

typedef unsigned long long bits64;
typedef long long sbits64;
typedef long long int64 ;
typedef unsigned long long uint64 ;

typedef signed int int32_t;
typedef unsigned int uint32_t;

typedef unsigned float32;
typedef unsigned long long float64;

    
#ifdef __cplusplus
}
#endif /*__cplusplus*/


#endif /*__FPTYPES_H__*/
