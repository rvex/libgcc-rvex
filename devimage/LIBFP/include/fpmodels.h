#ifndef __FPMODELS_H__
#define __FPMODELS_H__

/*------------------------------------------------------------------------------
Reference 'C' models of STMicroelectronics FP Intrinsics
This file is derived from fpmodels.src 
($Revision: 1.36.2.1 $ $Date: 2010/09/03 11:42:55 $).
STMicroelectronics Confidential.
------------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/


float32 __float32_add_float32_float32(float32 r0, float32 r1); 
float __float_add_float_float(float r0, float r1);

float32 __float32_add_fast_float32_float32(float32 r0, float32 r1); 

float32 __float32_sub_float32_float32(float32 r0, float32 r1); 
float __float_sub_float_float(float r0, float r1); 

float32 __float32_sub_fast_float32_float32(float32 r0, float32 r1); 

float32 __float32_mul_float32_float32(float32 r0, float32 r1); 
float __float_mul_float_float(float r0, float r1) ;

float32 __float32_mul_fast_float32_float32(float32 r0, float32 r1);

float32 __float32_madd_float32_float32_float32(float32 r0, float32 r1, float32 r2); 

float32 __float32_madd_fast_float32_float32_float32(float32 r0, float32 r1, float32 r2); 

float32 __float32_msub_float32_float32_float32(float32 r0, float32 r1, float32 r2);

float32 __float32_msub_fast_float32_float32_float32(float32 r0, float32 r1, float32 r2); 

float32 __float32_nmadd_float32_float32_float32(float32 r0, float32 r1, float32 r2);

float32 __float32_nmadd_fast_float32_float32_float32(float32 r0, float32 r1, float32 r2); 

float32 __float32_nmsub_float32_float32_float32(float32 r0, float32 r1, float32 r2);

float32 __float32_nmsub_fast_float32_float32_float32(float32 r0, float32 r1, float32 r2); 

int32 __int32_cmp_float32_float32(float32 r0, float32 r1); 
int32 __int32_cmp_float_float(float r0, float r1);

int32 __int32_eq_float32_float32(float32 r0, float32 r1); 
int32 __int32_eq_float_float(float r0, float r1);
int32 __int32_ne_float32_float32(float32 r0, float32 r1); 
int32 __int32_ne_float_float(float r0, float r1);

int32 __int32_le_float32_float32(float32 r0, float32 r1); 
int32 __int32_le_float_float(float r0, float r1);
int32 __int32_gt_float32_float32(float32 r0, float32 r1); 
int32 __int32_gt_float_float(float r0, float r1);

int32 __int32_lt_float32_float32(float32 r0, float32 r1); 
int32 __int32_lt_float_float(float r0, float r1);
int32 __int32_ge_float32_float32(float32 r0, float32 r1); 
int32 __int32_ge_float_float(float r0, float r1);

float32 __float32_min_float32_float32(float32 r0, float32 r1); 
float __float_min_float_float(float r0, float r1);

float32 __float32_max_float32_float32(float32 r0, float32 r1); 
float __float_max_float_float(float r0, float r1);

float32 __float32_div_float32_float32(float32 r0, float32 r1); 
float __float_div_float_float(float r0, float r1);

float32 __float32_div_fast_float32_float32(float32 r0, float32 r1); 

float32 __float32_sqrt_float32(float32 r0); 
float __float_sqrt_float(float r0);

float32 __float32_recip_float32(float32 r0); 
float __float_recip_float(float r0);

float32 __float32_square_float32(float32 r0); 
float __float_square_float(float r0);

float32 __float32_rsqrt_float32(float32 r0); 
float __float_rsqrt_float(float r0);

float32 __float32_from_int32( int32 r0 );
float __float_from_int32( int32 r0 );

float32 __float32_from_uint32( uint32 r0 );
float __float_from_uint32( uint32 r0 );

float32 __float32_from_int64( int64 r0 ); 
float __float_from_int64( int64 r0 ); 

float32 __float32_from_uint64( uint64 r0 ); 
float __float_from_uint64( uint64 r0 ); 

float32 __float32_from_int40( int40 r0 ); 
float __float_from_int40( int40 r0 );

float32 __float32_from_uint40( uint40 r0 );
float __float_from_uint40( uint40 r0 ); 

float32 __float32_from_fract32( fract32 r0 ); 
float32 __float32_from_fract16( fract16 r0 );

float32 __float32_from_fract40( fract40 r0 ); 

int32 __int32_from_float32( float32 r0 ); 
int32 __int32_from_float( float r0 ); 

int32 __int32_from_float32_rtz( float32 r0 ); 
int32 __int32_from_float_rtz( float r0 );

uint32 __uint32_from_float32( float32 r0 ); 
uint32 __uint32_from_float( float r0 ); 

uint32 __uint32_from_float32_rtz( float32 r0 );
uint32 __uint32_from_float_rtz( float r0 );

int40 __int40_from_float32( float32 r0 ); 
int40 __int40_from_float( float r0 ); 

int40 __int40_from_float32_rtz( float32 r0 ); 
int40 __int40_from_float_rtz( float r0 ); 

uint40 __uint40_from_float32( float32 r0 ); 
uint40 __uint40_from_float( float r0 );

uint40 __uint40_from_float32_rtz( float32 r0 ); 

int64 __int64_from_float32( float32 r0 ); 
int64 __int64_from_float( float r0 ); 

int64 __int64_from_float32_rtz( float32 r0 );
int64 __int64_from_float_rtz( float r0 ); 

uint64 __uint64_from_float32( float32 r0 ); 
uint64 __uint64_from_float( float r0 );

uint64 __uint64_from_float32_rtz( float32 r0 ) ;
uint64 __uint64_from_float_rtz( float r0 ); 

fract32 __fract32_from_float32( float32 r0 ); 

fract16 __fract16_from_float32( float32 r0 );

fract40 __fract40_from_float32( float32 r0 ); 
float32 __float32_round_to_int_float32( float32 r0 );

float64 __float64_add_float64_float64(float64 r0, float64 r1);
double __double_add_double_double(double r0, double r1); 

float64 __float64_add_fast_float64_float64(float64 r0, float64 r1); 

float64 __float64_sub_float64_float64(float64 r0, float64 r1); 
double __double_sub_double_double(double r0, double r1);

float64 __float64_sub_fast_float64_float64(float64 r0, float64 r1);

float64 __float64_mul_float64_float64(float64 r0, float64 r1); 
double __double_mul_double_double(double r0, double r1); 

float64 __float64_mul_fast_float64_float64(float64 r0, float64 r1); 

float64 __float64_div_float64_float64(float64 r0, float64 r1); 
double __double_div_double_double(double r0, double r1); 

float64 __float64_div_fast_float64_float64(float64 r0, float64 r1); 

float64 __float64_sqrt_float64(float64 r0); 

int32 __int32_cmp_float64_float64(float64 r0, float64 r1);
int32 __int32_cmp_double_double(double r0, double r1);

int32 __int32_eq_float64_float64(float64 r0, float64 r1); 
int32 __int32_eq_double_double(double r0, double r1);
int32 __int32_ne_float64_float64(float64 r0, float64 r1); 
int32 __int32_ne_double_double(double r0, double r1);

int32 __int32_le_float64_float64(float64 r0, float64 r1); 
int32 __int32_le_double_double(double r0, double r1);
int32 __int32_gt_float64_float64(float64 r0, float64 r1); 
int32 __int32_gt_double_double(double r0, double r1);

int32 __int32_lt_float64_float64(float64 r0, float64 r1); 
int32 __int32_lt_double_double(double r0, double r1);
int32 __int32_ge_float64_float64(float64 r0, float64 r1); 
int32 __int32_ge_double_double(double r0, double r1);

float64 __float64_min_float64_float64(float64 r0, float64 r1); 
double __double_min_double_double(double r0, double r1);

float64 __float64_max_float64_float64(float64 r0, float64 r1); 
double __double_max_double_double(double r0, double r1);

float64 __float64_from_float32(float32 r0); 
double __double_from_float(float r0);

float32 __float32_from_float64(float64 r0); 
float __float_from_double(double r0);

float64 __float64_from_int32( int32 r0 ); 
double __double_from_int32( int32 r0 ); 

float64 __float64_from_uint32( uint32 r0 ); 
double __double_from_uint32( uint32 r0 ); 

float64 __float64_from_int64( int64 r0 ); 
double __double_from_int64( int64 r0 ); 

float64 __float64_from_uint64( uint64 r0 ); 
double __double_from_uint64( uint64 r0 ); 

float64 __float64_from_int40( int40 r0 ); 
double __double_from_int40( int40 r0 );

float64 __float64_from_uint40( uint40 r0 );
double __double_from_uint40( uint40 r0 );

float64 __float64_from_fract32( fract32 r0 );

float64 __float64_from_fract16( fract16 r0 ); 

float64 __float64_from_fract40( fract40 r0 ); 

int32 __int32_from_float64( float64 r0 ); 
int32 __int32_from_double( double r0 ); 

int32 __int32_from_float64_rtz( float64 r0 ); 
int32 __int32_from_double_rtz( double r0 ); 

uint32 __uint32_from_float64( float64 r0 ); 
uint32 __uint32_from_double( double r0 ); 

uint32 __uint32_from_float64_rtz( float64 r0 ); 
uint32 __uint32_from_double_rtz( double r0 );

int40 __int40_from_float64( float64 r0 );
int40 __int40_from_double( double r0 ); 

int40 __int40_from_float64_rtz( float64 r0 );
int40 __int40_from_double_rtz( double r0 );

uint40 __uint40_from_float64( float64 r0 ); 
uint40 __uint40_from_double( double r0 ); 

uint40 __uint40_from_float64_rtz( float64 r0 ); 
uint40 __uint40_from_double_rtz( double r0 ); 

int64 __int64_from_float64( float64 r0 ); 
int64 __int64_from_double( double r0 );

int64 __int64_from_float64_rtz( float64 r0 ); 
int64 __int64_from_double_rtz( double r0 ); 

uint64 __uint64_from_float64( float64 r0 ); 
uint64 __uint64_from_double( double r0 ); 

uint64 __uint64_from_float64_rtz( float64 r0 ); 
uint64 __uint64_from_double_rtz( double r0 ); 

fract32 __fract32_from_float64( float64 r0 ); 
fract16 __fract16_from_float64( float64 r0 ); 
fract40 __fract40_from_float64( float64 r0 );

float64 __float64_round_to_int_float64( float64 r0 );


#ifdef __cplusplus
}
#endif /*__cplusplus*/


#endif /*__FPMODELS_H__*/
