#ifndef __ST220FPX_H__
#define __ST220FPX_H__


#ifdef __cplusplus
extern "C" {
#endif


float __q31tos(fract32);
float __q15tos(fract16);
fract32 __stoq31(float);
fract16 __stoq15(float);
double __q31tod(fract32);
double __q15tod(fract16);
fract32 __dtoq31(double);
fract16 __dtoq15(double);

#ifdef __cplusplus
}
#endif


#endif /*__ST220FPX_H__*/
