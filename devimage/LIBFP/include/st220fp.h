#ifndef __ST220FP_H__
#define __ST220FP_H__


#ifdef __cplusplus
extern "C" {
#endif


float __adds(float, float);
float __subs(float, float);
float __muls(float, float);
float __divs(float, float);
float __madds(float, float, float);
float __msubs(float, float, float);
float __nmadds(float, float, float);
float __nmsubs(float, float, float);
float __sqrts(float);
float __rsqrts(float);
float __recips(float);
float __squares(float);
float __rsqrts(float);
int32 __eqs(float, float);
int32 __nes(float, float);
int32 __les(float, float);
int32 __gts(float, float);
int32 __lts(float, float);
int32 __ges(float, float);
float __maxs(float, float);
float __mins(float, float);
float __wtos(int32);
float __uwtos(uint32);
float __ltos(int64);
float __ultos(uint64);
int32 __stow(float);
uint32 __stouw(float);
int64 __stol(float);
uint64 __stoul(float);
double __addd(double, double);
double __subd(double, double);
double __muld(double, double);
double __divd(double, double);
double __sqrtd(double);
int32 __eqd(double, double);
int32 __ned(double, double);
int32 __led(double, double);
int32 __gtd(double, double);
int32 __ltd(double, double);
int32 __ged(double, double);
double __maxd(double, double);
double __mind(double, double);
double __stod(float);
float __dtos(double);
double __wtod(int32);
double __uwtod(uint32);
double __ltod(int64);
double __ultod(uint64);
int32 __dtow(double);
uint32 __dtouw(double);
int64 __dtol(double);
uint64 __dtoul(double);

#ifdef __cplusplus
}
#endif


#endif /*__ST220FP_H__*/
