
.SUFFIXES:

CC = @tooldir@/rvex-gcc
AS = @tooldir@/rvex-as
AR = @tooldir@/rvex-ar
RANLIB = @tooldir@/rvex-ranlib
ISSUE = <|num_lanes|>

ST_FP = st/devimage/LIBFP/lib/libcfpi-st220.cc.a

.PHONY: all
all: libgcc.a

# Always chain to ST's makefile, so we don't have to hunt down all the
# dependencies here.
.PHONY: $(ST_FP)
$(ST_FP):
	$(MAKE) -C st libfp

VEXdiv.resched.s: @srcdir@/VEXdiv.resched.S
	cpp $^ -o $@ -DISSUE=$(ISSUE)

VEXdiv.o: VEXdiv.resched.s
	$(AS) $^ -o $@

libgcc.a: VEXdiv.o $(ST_FP)
	cp $(ST_FP) $@-tmp
	$(AR) r $@-tmp VEXdiv.o
	$(RANLIB) $@-tmp
	mv -f $@-tmp $@

.PHONY: install
install:
	mkdir -p "@prefix@/rvex-elf32/include"
	cp -rf -t "@prefix@/rvex-elf32/include" st/devimage/LIBFP/include/* st/devimage/LIBFLIP/include/*
	mkdir -p "@prefix@/rvex-elf32/lib"
	cp -f -t "@prefix@/rvex-elf32/lib" libgcc.a
