<!-- -*-xml-*- -->

<!DOCTYPE srcdoc PUBLIC "-//STMicroelectronics//DTD SourceDoc//EN" "srcdoc.dtd" [
    <!ENTITY name "st122-fp.src">
    <!ENTITY revision "$Revision: 1.1 $">
    <!ENTITY date "$Date: 2002/01/31 14:23:45 $">

    <!ENTITY ModelsToST122File	"fpmtost122">
    <!ENTITY ProtoFile 		"st122fp" >
    <!ENTITY ExtProtoFile 	"st122fpx" >
]>

<srcdoc id="st122-fp">

  <header>
    <title>ST122 Floating-Point Software Emulation Library</title>
    
    <author>
	<name>Herve Knochel</name>
    </author>

    <version>&revision; &date;</version>
  </header>


  <section><title>Introduction</title>
    
    <p>This document describes the st122 floating point software emulation library, developed in Linear Assembly Input language.</p>
    <p>For each operator, the LAI code corresponding to the C-model is produced.</p>

  </section>

  <section><title>Derived Files Prologue</title>

    <scrap name="redefine-direct">
#define <param1/> <param2/></scrap>

    <scrap name="prototype">
<param1/>;</scrap>

    <scrap name="begin-cplusplus">
#ifdef __cplusplus
extern "C" {
#endif
</scrap>

    <scrap name="end-cplusplus">
#ifdef __cplusplus
}
#endif
</scrap>

   <scrap emit="&ProtoFile;.h">
<expand scrap="begin-cplusplus"/>
</scrap>

    <scrap emit="&ExtProtoFile;.h">
<expand scrap="begin-cplusplus"/>
</scrap>

  </section>

  <section><title>Single precison operators</title>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __adds(float, float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_add_float_float" param2="__adds"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __subs(float, float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_sub_float_float" param2="__subs"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __muls(float, float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_mul_float_float" param2="__muls"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __divs(float, float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_div_float_float" param2="__divs"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __madds(float, float, float)"/></scrap>
    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __msubs(float, float, float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_madd_float_float_float" param2="__madds"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_msub_float_float_float" param2="__msubs"/></scrap>

    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_sqrt_float" param2="sqrtf"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __cmps(float, float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int32_cmp_float_float" param2="__cmps"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __wtos(int32)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_int32" param2="__wtos"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __uwtos(uint32)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_uint32" param2="__uwtos"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __etos(int40)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_int40" param2="__etos"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __uetos(int40)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_uint40" param2="__uetos"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __ltos(int64)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_int64" param2="__ltos"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __ultos(int64)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_uint64" param2="__ultos"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="float __q31tos(fract32)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_fract32" param2="__q31tos"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="float __q15tos(fract16)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_fract16" param2="__q15tos"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="float __a40tos(fract40)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_fract40" param2="__a40tos"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __stow(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int32_from_float_rtz" param2="__stow"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint32 __stouw(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__uint32_from_float_rtz" param2="__stouw"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int40 __stoe(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int40_from_float_rtz" param2="__stoe"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint40 __stoue(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__uint40_from_float_rtz" param2="__stoue"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int64 __stol(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int64_from_float_rtz" param2="__stol"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint64 __stoul(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__uint64_from_float_rtz" param2="__stoul"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract32 __stoq31(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__fract32_from_float" param2="__stoq31"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract16 __stoq15(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__fract16_from_float" param2="__stoq15"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract40 __stoa40(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__fract40_from_float" param2="__stoa40"/></scrap>

  </section>

  <section><title>Double precision operators</title>
    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __addd(double, double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_add_double_double" param2="__addd"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __subd(double, double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_sub_double_double" param2="__subd"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __muld(double, double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_mul_double_double" param2="__muld"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __divd(double, double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_div_double_double" param2="__divd"/></scrap>

    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_sqrt_double" param2="sqrt"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __cmpd(double, double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int32_cmp_double_double" param2="__cmpd"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __stod(float)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_float" param2="__stod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __dtos(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__float_from_double" param2="__dtos"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __wtod(int32)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_int32" param2="__wtod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __uwtod(uint32)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_uint32" param2="__uwtod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __etod(int40)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_int40" param2="__etod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __uetod(int40)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_uint40" param2="__uetod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __ltod(int64)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_int64" param2="__ltod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __ultod(int64)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_uint64" param2="__ultod"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="double __q31tod(fract32)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_fract32" param2="__q31tod"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="double __q15tod(fract16)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_fract16" param2="__q15tod"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="double __a40tod(fract40)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__double_from_fract40" param2="__a40tod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __dtow(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int32_from_double_rtz" param2="__dtow"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint32 __dtouw(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__uint32_from_double_rtz" param2="__dtouw"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int40 __dtoe(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int40_from_double_rtz" param2="__dtoe"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint40 __dtoue(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__uint40_from_double_rtz" param2="__dtoue"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int64 __dtol(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__int64_from_double_rtz" param2="__dtol"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint64 __dtoul(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__uint64_from_double_rtz" param2="__dtoul"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract32 __dtoq31(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__fract32_from_double" param2="__dtoq31"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract16 __dtoq15(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__fract16_from_double" param2="__dtoq15"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract40 __dtoa40(double)"/></scrap>
    <scrap emit="&ModelsToST122File;.h"><expand scrap="redefine-direct" param1="__fract40_from_double" param2="__dtoa40"/></scrap>

  </section>
   
  <section><title>Derived Files Epilogue</title>

    <scrap emit="&ProtoFile;.h">
<expand scrap="end-cplusplus"/>
</scrap>
    <scrap emit="&ExtProtoFile;.h">
<expand scrap="end-cplusplus"/>
</scrap>

  </section>

</srcdoc>