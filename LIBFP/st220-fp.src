<!-- -*-xml-*- -->

<!DOCTYPE srcdoc PUBLIC "-//STMicroelectronics//DTD SourceDoc//EN" "srcdoc.dtd" [
    <!ENTITY name "st220-fp.src">
    <!ENTITY revision "$Revision: 1.11 $">
    <!ENTITY date "$Date: 2009/09/01 11:56:46 $">

    <!ENTITY ModelsToST220File		"fpmtost220">
    <!ENTITY ModelsToST220-GCC-File	"fpmtost220gcc">
    <!ENTITY ProtoFile 			"st220fp" >
    <!ENTITY ExtProtoFile 		"st220fpx" >
]>

<srcdoc id="st220-fp">

  <header>
    <title>ST220 Floating-Point Software Emulation Library</title>
    
    <author>
	<name>Christophe Monat</name>
    </author>

    <version>&revision; &date;</version>
  </header>


  <section><title>Introduction</title>
    
    <p>This document describes the st220 floating point software emulation library, developed in Linear Assembly Input language.</p>
    <p>For each operator, the LAI code corresponding to the C-model is produced.</p>

  </section>

  <section><title>Derived Files Prologue</title>

    <scrap name="redefine-direct">
#define <param1/> <param2/></scrap>

    <scrap name="prototype">
<param1/>;</scrap>

    <scrap name="begin-cplusplus">
#ifdef __cplusplus
extern "C" {
#endif
</scrap>

    <scrap name="end-cplusplus">
#ifdef __cplusplus
}
#endif
</scrap>

   <scrap emit="&ProtoFile;.h">
<expand scrap="begin-cplusplus"/>
</scrap>

    <scrap emit="&ExtProtoFile;.h">
<expand scrap="begin-cplusplus"/>
</scrap>

  </section>

  <section><title>Single precison operators</title>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __adds(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_add_float_float" param2="__adds"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__float_add_float_float" param2="__addsf3"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __subs(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_sub_float_float" param2="__subs"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__float_sub_float_float" param2="__subsf3"/></scrap>


    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __muls(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_mul_float_float" param2="__muls"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__float_mul_float_float" param2="__mulsf3"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __divs(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_div_float_float" param2="__divs"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__float_div_float_float" param2="__divsf3"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __madds(float, float, float)"/></scrap>
    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __msubs(float, float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_madd_float_float_float" param2="__madds"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_msub_float_float_float" param2="__msubs"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __nmadds(float, float, float)"/></scrap>
    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __nmsubs(float, float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_nmadd_float_float_float" param2="__nmadds"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_nmsub_float_float_float" param2="__nmsubs"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __sqrts(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_sqrt_float" param2="__sqrts"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __rsqrts(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_rsqrt_float" param2="__rsqrts"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __recips(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_recip_float" param2="__recips"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __squares(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_square_float" param2="__squares"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __rsqrts(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_rsqrt_float" param2="__rsqrts"/></scrap>

    <!-- Three-way compares unused with current compiler technology
    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __cmps(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_cmp_float_float" param2="__cmps"/></scrap>
    -->

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __eqs(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_eq_float_float" param2="__eqs"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_eq_float_float" param2="__eqsf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __nes(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_ne_float_float" param2="__nes"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_ne_float_float" param2="__nesf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __les(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_le_float_float" param2="__les"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_le_float_float" param2="__lesf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __gts(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_gt_float_float" param2="__gts"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_gt_float_float" param2="__gtsf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __lts(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_lt_float_float" param2="__lts"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_lt_float_float" param2="__ltsf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __ges(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_ge_float_float" param2="__ges"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_ge_float_float" param2="__gesf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __maxs(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_max_float_float" param2="__maxs"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __mins(float, float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_min_float_float" param2="__mins"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __wtos(int32)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_from_int32" param2="__wtos"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__float_from_int32" param2="__floatsisf"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __uwtos(uint32)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_from_uint32" param2="__uwtos"/></scrap>   
    <!-- This one is handled in GCC by testing sign -->

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __ltos(int64)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_from_int64" param2="__ltos"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__float_from_int64" param2="__floatdisf"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __ultos(uint64)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_from_uint64" param2="__ultos"/></scrap>
    <!-- This one is handled in GCC by testing sign -->

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="float __q31tos(fract32)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_from_fract32" param2="__q31tos"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="float __q15tos(fract16)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_from_fract16" param2="__q15tos"/></scrap>
   
    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __stow(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_from_float_rtz" param2="__stow"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_from_float_rtz" param2="__fixsfsi"/></scrap>
    

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint32 __stouw(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__uint32_from_float_rtz" param2="__stouw"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__uint32_from_float_rtz" param2="__fixunssfsi"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int64 __stol(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int64_from_float_rtz" param2="__stol"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int64_from_float_rtz" param2="__fixsfdi"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint64 __stoul(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__uint64_from_float_rtz" param2="__stoul"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__uint64_from_float_rtz" param2="__fixunssfdi"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract32 __stoq31(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__fract32_from_float" param2="__stoq31"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract16 __stoq15(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__fract16_from_float" param2="__stoq15"/></scrap>

  </section>

  <section><title>Double precision operators</title>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __addd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_add_double_double" param2="__addd"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__double_add_double_double" param2="__adddf3"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __subd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_sub_double_double" param2="__subd"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__double_sub_double_double" param2="__subdf3"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __muld(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_mul_double_double" param2="__muld"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__double_mul_double_double" param2="__muldf3"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __divd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_div_double_double" param2="__divd"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__double_div_double_double" param2="__divdf3"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __sqrtd(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_sqrt_double" param2="__sqrtd"/></scrap>

    <!-- Three-way compares unused with current compiler technology
    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __cmpd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_cmp_double_double" param2="__cmpd"/></scrap>
    -->

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __eqd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_eq_double_double" param2="__eqd"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_eq_double_double" param2="__eqdf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __ned(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_ne_double_double" param2="__ned"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_ne_double_double" param2="__nedf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __led(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_le_double_double" param2="__led"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_le_double_double" param2="__ledf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __gtd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_gt_double_double" param2="__gtd"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_gt_double_double" param2="__gtdf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __ltd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_lt_double_double" param2="__ltd"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_lt_double_double" param2="__ltdf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __ged(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_ge_double_double" param2="__ged"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_ge_double_double" param2="__gedf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __maxd(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_max_double_double" param2="__maxd"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __mind(double, double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_min_double_double" param2="__mind"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __stod(float)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_from_float" param2="__stod"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__double_from_float" param2="__extendsfdf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="float __dtos(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__float_from_double" param2="__dtos"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__float_from_double" param2="__truncdfsf2"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __wtod(int32)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_from_int32" param2="__wtod"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__double_from_int32" param2="__floatsidf"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __uwtod(uint32)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_from_uint32" param2="__uwtod"/></scrap>
    <!-- This one is handled in GCC by testing sign -->

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __ltod(int64)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_from_int64" param2="__ltod"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__double_from_int64" param2="__floatdidf"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="double __ultod(uint64)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_from_uint64" param2="__ultod"/></scrap>
    <!-- This one is handled in GCC by testing sign -->

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="double __q31tod(fract32)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_from_fract32" param2="__q31tod"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="double __q15tod(fract16)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__double_from_fract16" param2="__q15tod"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int32 __dtow(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int32_from_double_rtz" param2="__dtow"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int32_from_double_rtz" param2="__fixdfsi"/></scrap>
    

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint32 __dtouw(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__uint32_from_double_rtz" param2="__dtouw"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__uint32_from_double_rtz" param2="__fixunsdfsi"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="int64 __dtol(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__int64_from_double_rtz" param2="__dtol"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__int64_from_double_rtz" param2="__fixdfdi"/></scrap>

    <scrap emit="&ProtoFile;.h"><expand scrap="prototype" param1="uint64 __dtoul(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__uint64_from_double_rtz" param2="__dtoul"/></scrap>
    <scrap emit="&ModelsToST220-GCC-File;.h"><expand scrap="redefine-direct" param1="__uint64_from_double_rtz" param2="__fixunsdfdi"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract32 __dtoq31(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__fract32_from_double" param2="__dtoq31"/></scrap>

    <scrap emit="&ExtProtoFile;.h"><expand scrap="prototype" param1="fract16 __dtoq15(double)"/></scrap>
    <scrap emit="&ModelsToST220File;.h"><expand scrap="redefine-direct" param1="__fract16_from_double" param2="__dtoq15"/></scrap>

  </section>
   
  <section><title>Derived Files Epilogue</title>

    <scrap emit="&ProtoFile;.h">
<expand scrap="end-cplusplus"/>
</scrap>
    <scrap emit="&ExtProtoFile;.h">
<expand scrap="end-cplusplus"/>
</scrap>

  </section>

</srcdoc>