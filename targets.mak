# -*-Makefile-*-
#
help:
	@echo
	@echo
	@echo "specify one of : conf all-conf all clean distclean"
	@echo 
	@echo "libfp (conf- all- clean-)"
	@echo "libflip (conf- all- clean-)"
	@echo

##
# Building all targets
#
all: libfp libflip

##
# Cleaning all targets
#
clean: clean-idsp clean-libc clean-libfp clean-libflip clean-libm clean-unwind clean-misc clean-notes clean-doc clean-libgcc

##
# Clean everything
distclean: distclean-libfp distclean-libflip clean-conf clean-dist

##
# Building  FP runtime support library
#
LIBFP_ALL=LIBFP
LIBFP_TST=TST/LIBFP/BUG TST/LIBFP/UNIT
LIBFP_PRF=PRF/LIBFP/TFLT
libfp:
	$(MAKE) conf-libfp || exit 1
	$(MAKE) all-libfp || exit 1

conf-libfp:
	$(MAKE) SUBS="$(LIBFP_ALL)" subs-conf || exit 1
	$(MAKE) SUBS="$(LIBFP_TST)" subs-conf || exit 1
	$(MAKE) SUBS="$(LIBFP_PRF)" subs-conf || exit 1

all-libfp:
	$(MAKE) SUBS="$(LIBFP_ALL)" TARGETS="all install" subs-make || exit 1

clean-libfp:
	$(MAKE) SUBS="$(LIBFP_ALL)" TARGETS="clean" subs-make || exit 1

distclean-libfp:
	$(MAKE) SUBS="$(LIBFP_ALL)" TARGETS="distclean" subs-distclean || exit 1
	$(MAKE) SUBS="$(LIBFP_TST)" TARGETS="distclean" subs-distclean || exit 1
	$(MAKE) SUBS="$(LIBFP_PRF)" TARGETS="distclean" subs-distclean || exit 1

##
# Building  FLIP runtime support library
#
LIBFLIP_ALL=LIBFLIP
libflip:
	$(MAKE) conf-libflip || exit 1
	$(MAKE) all-libflip || exit 1

conf-libflip:
	$(MAKE) SUBS="$(LIBFLIP_ALL)" subs-conf || exit 1

all-libflip:
	$(MAKE) SUBS="$(LIBFLIP_ALL)" TARGETS="all install" subs-make || exit 1

clean-libflip:
	$(MAKE) SUBS="$(LIBFLIP_ALL)" TARGETS="clean" subs-make || exit 1

distclean-libflip:
	$(MAKE) SUBS="$(LIBFLIP_ALL)" TARGETS="distclean" subs-distclean || exit 1


##
# Building libgcc
LIBGCCS = libgcc.a 
ifeq ("$(TARGET_OS)", "linux")
LIBGCCS += libgcc_eh.a
LIBGCCS += libgcc_s.so libgcc_s.so.1
endif

libgcc: all-libgcc install-libgcc

all-libgcc: $(LIBGCCS)

clean-libgcc:
	rm -f $(LIBGCCS)

distclean-libgcc: clean-libgcc

install-libgcc:
	$(INSTALL_DIR) $(REF_PREFIX)/LIBGCC/lib || exit 1
	for lib in $(LIBGCCS); \
	do \
		$(INSTALL_DATA) $$lib $(REF_PREFIX)/LIBGCC/lib; \
	done

LIBGCCDEPS= libfp
LIBGCCSUBS= $(LIBFP_ALL)
ifeq ("$(TARGET_OS)", "linux")
LIBGCCDEPS +=
LIBGCCSUBS +=
endif

libgcc.a: $(LIBGCCDEPS)
	mkdir -p libgcc-tmp || exit 1
	( cd libgcc-tmp || exit 1; \
	for sub in $(LIBGCCSUBS) ; \
	do \
	  if [ -f $(SRC_PREFIX)/$$sub/libgcc-contrib.a ]; \
	  then \
	    $(AR) -x $(SRC_PREFIX)/$$sub/libgcc-contrib.a; \
	  fi; \
	done; \
	cd .. )
	$(AR) rc $@ libgcc-tmp/*.o
	$(RL) $@
	rm -rf libgcc-tmp

libgcc_eh.a: $(LIBGCCDEPS)
	mkdir -p libgcc-tmp || exit 1
	( cd libgcc-tmp || exit 1; \
	for sub in $(LIBGCCSUBS) ; \
	do \
	  if [ -f $(SRC_PREFIX)/$$sub/libgcc_eh-contrib.a ]; \
	  then \
	    $(AR) -x $(SRC_PREFIX)/$$sub/libgcc_eh-contrib.a; \
	  fi; \
	done; \
	cd .. )
	$(AR) rc $@ libgcc-tmp/*.o
	$(RL) $@
	rm -rf libgcc-tmp

libgcc_s.so.1: idsp libfp misc unwind
	mkdir -p libgcc_s-tmp || exit 1
	( cd libgcc_s-tmp || exit 1; \
	for sub in $(LIBFP_ALL); \
	do \
	  if [ -f $(SRC_PREFIX)/$$sub/libgcc_s-contrib.a ]; \
	  then \
	    $(AR) -x $(SRC_PREFIX)/$$sub/libgcc_s-contrib.a || exit 1; \
	  fi; \
	done; \
	$(CC) -shared -nostdlib -soname $@ -o libgcc.so *.o || exit 1; \
	cp -p libgcc.so ../libgcc_s.so.1 || exit 1; \
	cd .. )
#	rm -rf libgcc_s-tmp

libgcc_s.so: $(SRC_PREFIX)/LIBGCC/libgcc_s.so
	$(INSTALL_DATA) $(SRC_PREFIX)/LIBGCC/libgcc_s.so .

##
# Configuration related stuff
#
clean-conf:
	rm -f *.def config.* conf.mk

conf:
	$(SRC_PREFIX)/$(CONFIG)/configure --prefix=$(REF_PREFIX) --exec_prefix=$(REF_EXEC_PREFIX) \
	--with-target-cc-path=$(CNF_WITH_TARGET_CC_PATH) --with-target-cc-name=$(CNF_WITH_TARGET_CC_NAME) \
	--with-target-cxx-path=$(CNF_WITH_TARGET_CXX_PATH) --with-target-cxx-name=$(CNF_WITH_TARGET_CXX_NAME) \
	--with-target-cxx-abi=$(CNF_WITH_TARGET_CXX_ABI) \
	--with-target-binutils-home=$(CNF_WITH_TARGET_BINUTILS_HOME) --with-target-runner-home=$(CNF_WITH_TARGET_RUNNER_HOME) \
	--with-target-simulator-home=$(CNF_WITH_TARGET_SIMULATOR_HOME) --with-target-simulator-name=$(CNF_WITH_TARGET_SIMULATOR_NAME) \
	--enable-perf=$(TARGET_ENABLE_PERF) --enable-debug=$(TARGET_ENABLE_DEBUG) \
	--enable-endian=$(TARGET_ENDIANNESS) --enable-core=$(TARGET_CORE) \
	--build=$(CNF_BUILD) --host=$(CNF_HOST) --target=$(CNF_TARGET) || exit 1

all-conf: conf-idsp conf-libc conf-libc++ conf-libfp conf-libm conf-notes conf-doc conf-libflip conf-unwind conf-misc

##
# Generic targets

subs-distclean:
	@for dir in $(SUBS); \
	do \
	  if [ -f $(SRC_PREFIX)/$$dir/Makefile ]; \
	  then \
	  $(MAKE) SUB=$$dir sub-distclean || exit 1; \
	  fi; \
	done

##
# Configure subcomponents
subs-%:
	@for dir in $(SUBS); \
	do \
	  if [ -d $(SRC_PREFIX)/$$dir ]; \
	  then \
	  $(MAKE) SUB=$$dir sub-$* || exit 1; \
	  fi; \
	done

##
# Configure a subdir
# Expect SUB to contain the component name
sub-conf:
	@echo
	@echo "   Configure for $(SUB)"
	@echo
	@-if test ! -d $(SUB); then	\
		mkdir -p $(SUB);	\
	fi
	echo $(MAKE)
	-cd $(SUB);  MAKEDEF=$(CNF_PREFIX)/conf.mk TARGETDEF=$(CNF_PREFIX)/$(CNF_ARCHI)-tools.mk $(SRC_PREFIX)/$(SUB)/configure \
	--with-target-cc-path=$(CNF_WITH_TARGET_CC_PATH) --with-target-cc-name=$(CNF_WITH_TARGET_CC_NAME) \
	--with-target-cxx-path=$(CNF_WITH_TARGET_CXX_PATH) --with-target-cxx-name=$(CNF_WITH_TARGET_CXX_NAME) \
	--with-target-cxx-abi=$(CNF_WITH_TARGET_CXX_ABI) \
	--with-target-binutils-home=$(CNF_WITH_TARGET_BINUTILS_HOME) --with-target-runner-home=$(CNF_WITH_TARGET_RUNNER_HOME) \
	--with-target-simulator-home=$(CNF_WITH_TARGET_SIMULATOR_HOME) --with-target-simulator-name=$(CNF_WITH_TARGET_SIMULATOR_NAME) \
	--enable-perf=$(TARGET_ENABLE_PERF) --enable-debug=$(TARGET_ENABLE_DEBUG) \
	--enable-endian=$(TARGET_ENDIANNESS) --enable-core=$(TARGET_CORE) \
	--prefix=$(REF_PREFIX) --exec_prefix=$(REF_EXEC_PREFIX) \
	--build=$(CNF_BUILD) --host=$(CNF_HOST) --target=$(CNF_TARGET)

##
# Make in subdir
# Expects SUB to contain the subdir and TARGETS the targets
sub-make:
	@echo
	@echo "   Make $(TARGETS) for $(SUB)"
	@echo
	cd $(SUB); $(MAKE) $(TARGETS)

##
# Make in subdir
# Expects SUB to contain the subdir and TARGETS the targets
sub-distclean:
	@echo
	@echo "   Distclean $(TARGETS) for $(SUB)"
	@echo
	-cd $(SUB); $(MAKE) $(TARGETS)

##
# Tag 
tag:
	if [ "$(TAG)" = "" ] ; then \
	  echo "Specify TAG variable" >&2; \
	  exit 1; \
	fi
	cd $(SRC_PREFIX); cvs tag -R $(TAG) .

##
# Make the distribution tar file
#
dist:
	if [ "$(TAG)" = "" ] ; then \
	  echo "Specify TAG variable" >&2; \
	  exit 1; \
	fi
	if test ! -d $(DIST) ; then \
	  mkdir -p $(DIST) ; \
	fi
	if test ! -f $(SRC_PREFIX)/CVS/Root; then \
	  echo "Cannot find CVS root" >& 2; \
	fi
	( cd $(DIST); \
	cvs -d `cat $(SRC_PREFIX)/CVS/Root` export -r $(TAG) IFR; \
	tar cvf IFR-$(TAG).tar IFR; \
	gzip IFR-$(TAG).tar) 

clean-dist:
	rm -rf $(DIST)
