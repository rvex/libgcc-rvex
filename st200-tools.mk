#-*-Makefile-*-

.PRECIOUS: .s

#--------------------------------------------
# Definition of tools needed to build executables
# is done at the configure level

#--------------------------------------------
# 1) TCG C Compiler
# This part can be modified to define where to find TCG
# TARGET_LCC_PATH comes through configuration
TCG_PATH = $(TARGET_LCC_PATH)
TCG_CC_NAME = $(TARGET_LCC_NAME)

TCG_CC = $(TCG_PATH)/$(TCG_CC_NAME) 

TCG_CC_DEFS = -D__Has_Float_IOs -D__STDC_HOSTED__ -DNDEBUG
TCG_CC_INCS = -nostdinc
TCG_CC_CONF = -mlittle-endian #-mcpu=${target} 
ifeq ("$(TARGET_ENDIANNESS)", "be")
TCG_CC_CONF = -mbig-endian #-mcpu=${target} 
endif
ifeq ("$(TARGET_ABI)", "pic-caller-sets-gp")
PICOPT=-fpic-caller-sets-gp
else
PICOPT=
endif
ifeq ("$(TARGET_OS)", "linux")
# Linux wants PIC libraries
TCG_CC_CONF += $(PICOPT)
endif
TCG_CC_OPTS = -O3

TCG_CC_CCOPTS = $(TCG_CC_DEFS) $(TCG_CC_INCS) $(TCG_CC_CONF) $(TCG_CC_OPTS)

#--------------------------------------------
# 2) Perofmance C Compiler
# This part can be modified to define where to find TCG
# This part should not be modified
# CC_PATH comes through configuration
CC_PATH = $(TARGET_CC_PATH)
CC_NAME = $(TARGET_CC_NAME)
CC = $(CC_PATH)/$(CC_NAME)

CC_DEFS = -D__Has_Float_IOs -DNDEBUG
CC_INCS = -nostdinc
CC_CONF = #-mcore=$(TARGET_CORE)
ifeq ("$(TARGET_ENDIANNESS)", "le")
CC_CONF += -EL
else
CC_CONF += -EB
endif
ifeq ("$(TARGET_OS)", "linux")
CC_CONF += $(PICOPT)
endif

ifeq ("$(TARGET_CORE)", "st235")
#CC_CONF += -TARG:enable_64bits=OFF -TENV:emulate_fp=ON
endif

#CC_OPTS = -Os 
#CC_OPTS = -O2 # Was used for a long time.
# -O3 is underpefroming unless we disable the following optimization
# that is detrimental for all adaptation functions 
# (inlined body of code in type adaptation layer)
CC_OPTS = -O3 -WOPT:goto=0 -CG:ifc_factor=3.0 -CG:LAO_activation=0
#-PHASE:lno 
#-OPT:cis=OFF -OPT:roundoff=2 -OPT:IEEE_arithmetic=1
#-PHASE:lno #-LNO:opt=0
# X=0 No speculation allowed ?
#-CG:loop_opt -WOPT:lftr2=1 -PHASE:lno
#-CG:ssa_opt=ON -CG:select_if_convert=ON  -CG:select_allow_dup=ON -TENV:X=4
#CC_OPTS = -O -CG:ebo_level=0 -CG:local_scheduler=OFF
#CC_OPTS = -O0
#-CG:ssa_algorithm=2

CC_CCOPTS = $(CC_DEFS) $(CC_INCS) $(CC_CONF) $(CC_OPTS)


#--------------------------------------------
# 2 Bis) C++ compiler
# This part can be modified to define where to find TCG
# This part should not be modified
# CC_PATH comes through configuration
CXX_PATH = $(TARGET_CXX_PATH)
CXX_NAME = $(TARGET_CXX_NAME)
CXX = $(CXX_PATH)/$(CXX_NAME)

CXX_DEFS = -D__Has_Float_IOs -DNDEBUG
CXX_INCS = -nostdinc
CXX_CONF = #-mcore=$(TARGET_CORE)
ifeq ("$(TARGET_ENDIANNESS)", "le")
CXX_CONF += -EL
else
CXX_CONF += -EB
endif
ifeq ("$(TARGET_OS)", "linux")
CXX_CONF += $(PICOPT)
endif

CXX_OPTS = -O2 -fno-rtti -fno-exceptions 

CXX_CCOPTS = $(CXX_DEFS) $(CXX_INCS) $(CXX_CONF) $(CXX_OPTS)

#--------------------------------------------
# 3) Binary Utilities
AS=$(TARGET_BINUTILS_HOME)/$(TOOLNAME_ROOT)as
# This is needed when building for I-cache optimizations
ASOPTS = -L #--emit-all-relocs
ASOPTS += #-mcore=$(TARGET_CORE)
ifeq ("$(TARGET_ENDIANNESS)", "le")
ASOPTS += -EL 
else
ASOPTS += -EB
endif
HAVE_32x32_DIV=1
HAVE_32x32_MUL=1
HAVE_16x32_MUL=0
HAVE_DIVS=0
HAVE_MULFRAC=1
HAVE_PRGINS=0
HAVE_PRGINSSET=1
HAVE_SAT_ARITH=1
ifeq ("$(TARGET_CORE)", "st220")
HAVE_32x32_DIV=0
HAVE_32x32_MUL=0
HAVE_16x32_MUL=1
HAVE_DIVS=1
HAVE_MULFRAC=0
HAVE_PRGINS=1
HAVE_PRGINSSET=0
HAVE_SAT_ARITH=0
endif
ifeq ("$(TARGET_CORE)", "st231")
HAVE_32x32_DIV=0
HAVE_16x32_MUL=1
HAVE_DIVS=1
HAVE_PRGINS=1
HAVE_PRGINSSET=0
HAVE_SAT_ARITH=0
endif
ifeq ("$(TARGET_CORE)", "st239")
HAVE_32x32_DIV=0
HAVE_PRGINS=1
HAVE_PRGINSSET=0
HAVE_SAT_ARITH=0
endif
ASOPTS += --defsym have_32x32_div=$(HAVE_32x32_DIV)
ASOPTS += --defsym have_32x32_mul=$(HAVE_32x32_MUL)
ASOPTS += --defsym have_16x32_mul=$(HAVE_16x32_MUL)
ASOPTS += --defsym have_divs=$(HAVE_DIVS)
ASOPTS += --defsym have_mulfrac=$(HAVE_MULFRAC)
ASOPTS += --defsym have_prgins=$(HAVE_PRGINS)
ASOPTS += --defsym have_prginsset=$(HAVE_PRGINSSET)
ASOPTS += --defsym have_sat_arith=$(HAVE_SAT_ARITH)
AR=$(TARGET_BINUTILS_HOME)/$(TOOLNAME_ROOT)ar
RL=$(TARGET_BINUTILS_HOME)/$(TOOLNAME_ROOT)ranlib
LDOPTS =
ifeq ("$(TARGET_ENDIANNESS)", "le")
LDOPTS += -EL
else
LDOPTS += -EB
endif
LD=$(TARGET_BINUTILS_HOME)/$(TOOLNAME_ROOT)ld

#--------------------------------------------
# 4) Runner
RUNNER = $(TARGET_RUNNER_HOME)/bin/st200run
RUNOPTS = --dll="$(TARGET_SIMULATOR_HOME)/$(TARGET_SIMULATOR_NAME) MODE FAST"
ifeq ("$(TARGET_CORE)", "st235")
RUNOPTS += -f
endif
ifeq ("$(TARGET_CORE)", "st239")
RUNOPTS += -f
endif
ifeq ("$(TARGET_CORE)", "st240")
RUNOPTS += -f
endif
RUNOPTSREF = --dll="$(TARGET_SIMULATOR_HOME)/$(TARGET_SIMULATOR_NAME) MODE REFERENCE HAZARD_CHECKING_ON TRUE BUNDLE_CHECKING_ON TRUE"
RUN = $(RUNNER) $(RUNOPTS) --
RUNREF= $(RUNNER) $(RUNOPTSREF) --

#--------------------------------------------
# 4) srccode tool
ifeq ("$(TARGET_ABI)", "pic-caller-sets-gp")
ALL_SRCCODEFLAGS += -V pic-caller-sets-gp
endif

#--------------------------------------------
# This fragment defines common rules
# It is rather convoluted and should not be modified 
# When one simply passes tests no when one adds a test
# It should be update d when adding a new kind of toolset 
# Such as a new compiler

#--------------------------------------------
# Clear default rules is assumed to be done a higher level

#-------------------
# C Oriented rules
# Ruling from .s(C) to .o when produced by bootstrap compiler
# Note that we go form .c to .o by .s and that we need an intermediary
# target to control the assembler we pass we
%.c.tcg.s : %.c 
	$(TCG_CC) $(TCG_CC_CCOPTS) -S $^ -o $@

# Ruling from .s(C) to .o when produced by performance compiler
%.c.cc.s : %.c 
	$(CC) $(CC_CCOPTS) -S $^ -o $@

#-------------------
# C++ Oriented rules

# Ruling from .s(C) to .o when produced by performance compiler
%.cxx.cc.s : %.cxx
	$(CXX) $(CXX_CCOPTS) -S $^ -o $@

#-------------------
# ASM Oriented rules
%.o : %.s
	$(AS) $(ASOPTS) $^ -o $@
